// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    sid: json['sid'] as String,
    title: json['title'] as String,
    site: json['site'] as String,
    price: (json['price'] as num)?.toDouble(),
    delay: json['delay'] as int,
  );
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'sid': instance.sid,
      'title': instance.title,
      'site': instance.site,
      'price': instance.price,
      'delay': instance.delay,
    };
