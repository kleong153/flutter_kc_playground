import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'product.g.dart';

///
/// REF: Command for generate .g.dart file:
/// flutter packages pub run build_runner build
///
@JsonSerializable()
class Product extends Equatable {
  final String sid;
  final String title;
  final String site;
  final double price;
  final int delay;

  Product({this.sid, this.title, this.site, this.price, this.delay});

  @override
  List<Object> get props => [sid, title, site, price];

  factory Product.fromJson(Map<String, dynamic> json) => _$ProductFromJson(json);

  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
