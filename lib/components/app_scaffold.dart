import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class AppScaffold extends StatelessWidget {
  final String titleText;
  final Widget body;

  const AppScaffold({
    @required this.titleText,
    @required this.body,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(titleText),
      ),
      body: body,
    );
  }
}
