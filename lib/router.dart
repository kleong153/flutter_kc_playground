import 'package:flutter/material.dart';
import 'package:flutter_kc_playground/screens/cached_list/cached_list_screen.dart';
import 'package:flutter_kc_playground/screens/db_viewer/db_viewer_screen.dart';
import 'package:flutter_kc_playground/screens/home/home_screen.dart';
import 'package:flutter_kc_playground/screens/route_not_found/route_not_found_screen.dart';
import 'package:flutter_kc_playground/screens/start_native_screen/start_native_screen.dart';
import 'package:flutter_kc_playground/screens/stream_list/stream_list_screen.dart';

class Router {
  static const String home = '/';
  static const String streamList = '/stream_list';
  static const String cachedList = '/cached_list';
  static const String startNativeScreen = '/start_native_screen';
  static const String dbViewer = '/db_viewer';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    Widget widget;

    switch (settings.name) {
      case home:
        widget = HomeScreen();
        break;
      case streamList:
        widget = StreamListScreen();
        break;
      case cachedList:
        widget = CachedListScreen();
        break;
      case startNativeScreen:
        widget = StartNativeScreen();
        break;
      case dbViewer:
        widget = DbViewerScreen();
        break;
      default:
        widget = RouteNotFoundScreen();
        break;
    }

    return _materialPageRoute(widget);
  }

  static MaterialPageRoute _materialPageRoute(Widget widget) {
    return MaterialPageRoute(builder: (_) => widget);
  }
}
