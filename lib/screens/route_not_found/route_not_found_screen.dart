import 'package:flutter/material.dart';
import 'package:flutter_kc_playground/components/app_scaffold.dart';

class RouteNotFoundScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      titleText: 'Page Not Found',
      body: Center(
        child: Text('Page not found.'),
      ),
    );
  }
}
