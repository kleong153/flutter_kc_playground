import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_kc_playground/blocs/cached_list/cached_list_bloc.dart';
import 'package:flutter_kc_playground/blocs/cached_list/cached_list_event.dart';
import 'package:flutter_kc_playground/blocs/cached_list/cached_list_state.dart';
import 'package:flutter_kc_playground/components/app_scaffold.dart';
import 'package:flutter_kc_playground/database/app_database.dart';
import 'package:flutter_kc_playground/screens/cached_list/components/body_widget.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

///
/// REF: https://github.com/peng8350/flutter_pulltorefresh/blob/master/README_CN.md
///
class CachedListScreen extends StatelessWidget {
  final RefreshController _refreshController = RefreshController(initialRefresh: false);

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      titleText: 'Cached List',
      body: BlocProvider(
        create: _blocProvider,
        child: BlocConsumer<CachedListBloc, CachedListState>(
          listener: _blocConsumerListener,
          buildWhen: (previous, current) {
            // Ignore Loading state.
            return !(current is CachedListLoading);
          },
          builder: (context, state) {
            return BodyWidget(
              refreshController: _refreshController,
              bloc: context.bloc<CachedListBloc>(),
              blocState: state,
            );
          },
        ),
      ),
    );
  }

  CachedListBloc _blocProvider(BuildContext context) {
    return CachedListBloc(
      httpClient: http.Client(),
      appDatabase: Provider.of<AppDatabase>(context, listen: false),
    )..add(CachedListNextPage());
  }

  void _blocConsumerListener(BuildContext context, CachedListState state) {
    // TODO
    print('d;;CachedListScreen BlocConsumer listener state');
    print(state);

    if (state is CachedListSuccess || state is CachedListFailure) {
      if (_refreshController.isRefresh) {
        _refreshController.refreshCompleted();
      }

      if (_refreshController.isLoading) {
        _refreshController.loadComplete();
      }
    }
  }
}
