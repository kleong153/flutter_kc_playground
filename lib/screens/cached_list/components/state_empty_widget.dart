import 'package:flutter/material.dart';

class StateEmptyWidget extends StatelessWidget {
  final String text;

  const StateEmptyWidget({
    Key key,
    @required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text(text),
      ),
    );
  }
}
