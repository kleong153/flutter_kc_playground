import 'package:flutter/material.dart';
import 'package:flutter_kc_playground/blocs/cached_list/cached_list_bloc.dart';
import 'package:flutter_kc_playground/blocs/cached_list/cached_list_event.dart';
import 'package:flutter_kc_playground/blocs/cached_list/cached_list_state.dart';
import 'package:flutter_kc_playground/screens/cached_list/components/list_item_widget.dart';
import 'package:flutter_kc_playground/screens/cached_list/components/state_empty_widget.dart';
import 'package:flutter_kc_playground/screens/cached_list/components/state_initial_widget.dart';
import 'package:meta/meta.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class BodyWidget extends StatelessWidget {
  final RefreshController refreshController;
  final CachedListBloc bloc;
  final CachedListState blocState;
  final _textController = TextEditingController();

  BodyWidget({
    Key key,
    @required this.refreshController,
    @required this.bloc,
    @required this.blocState,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (blocState is CachedListInitial) {
      return StateInitialWidget();
    }

    return Column(
      children: <Widget>[
        _demoSearchWidget(),
        Expanded(
          child: SmartRefresher(
            enablePullDown: true,
            enablePullUp: _enablePullUp(blocState),
            onRefresh: () => _onRefresh(context),
            onLoading: () => _onLoading(context),
            controller: refreshController,
            header: ClassicHeader(),
            footer: ClassicFooter(),
            child: _getContentWidgetByState(blocState),
          ),
        ),
      ],
    );
  }

  Widget _demoSearchWidget() {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 4,
            child: TextField(
              controller: _textController,
              decoration: InputDecoration(hintText: 'Search...'),
            ),
          ),
          Flexible(
            child: RaisedButton(
              child: Text('Search'),
              onPressed: () {
                refreshController.requestRefresh();
              },
            ),
          ),
        ],
      ),
    );
  }

  bool _enablePullUp(CachedListState state) {
    return (state is CachedListSuccess && !state.hasReachedMax);
  }

  void _onRefresh(BuildContext context) async {
    bloc.add(CachedListRefresh(search: _textController.text));
  }

  void _onLoading(BuildContext context) async {
    bloc.add(CachedListNextPage());
  }

  Widget _getContentWidgetByState(CachedListState state) {
    // TODO cannot move this block of code to class otherwise pull refresh will not work.
    print('d;;CachedListScreen _getContentWidgetByState state');
    print(state);

    if (state is CachedListFailure) {
      // Load data failed.
      return StateEmptyWidget(
        text: 'Failed to load data.',
      );
    } else if (state is CachedListSuccess) {
      if (state.products.isEmpty) {
        // Data is empty.
        return StateEmptyWidget(
          text: 'No data.',
        );
      }

      // Has data.
      return ListView.builder(
        itemCount: state.products.length,
        itemBuilder: (context, index) => ListItemWidget(product: state.products[index]),
      );
    } else {
      throw Exception('Unknown state: $state');
    }
  }
}
