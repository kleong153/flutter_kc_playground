import 'package:flutter/material.dart';
import 'package:flutter_kc_playground/models/product.dart';
import 'package:meta/meta.dart';

class ListItemWidget extends StatelessWidget {
  final Product product;

  const ListItemWidget({Key key, @required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Row(
          children: <Widget>[
            Container(
              color: Colors.blueGrey,
              height: 80.0,
              width: 80.0,
              child: Center(
                child: Text(
                  product.site,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    product.title,
                    maxLines: 1,
                    softWrap: true,
                    textAlign: TextAlign.left,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    'RM ${product.price.toStringAsFixed(2)}',
                    maxLines: 1,
                    softWrap: true,
                    textAlign: TextAlign.left,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Text(
                    '(Load seconds: ${product.delay})',
                    maxLines: 1,
                    softWrap: true,
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
