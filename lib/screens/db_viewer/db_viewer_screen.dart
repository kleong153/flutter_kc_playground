import 'package:flutter/material.dart';
import 'package:flutter_kc_playground/database/app_database.dart';
import 'package:moor_db_viewer/moor_db_viewer.dart';
import 'package:provider/provider.dart';

///
/// REF: GITHUB:
/// https://github.com/vanlooverenkoen/moor_db_viewer
///
class DbViewerScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MoorDbViewer(Provider.of<AppDatabase>(context));
  }
}
