import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_kc_playground/components/app_scaffold.dart';

class StartNativeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      titleText: 'Start Native Screen',
      body: _BodyBuilder(),
    );
  }
}

class _BodyBuilder extends StatefulWidget {
  @override
  State<_BodyBuilder> createState() => _BodyState();
}

class _BodyState extends State<_BodyBuilder> {
  final _textController = TextEditingController();
  String _responseText = "";

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20.0),
      child: Column(
        children: <Widget>[
          TextField(
            controller: _textController,
            decoration: InputDecoration(
              hintText: 'Input anything here...',
            ),
          ),
          RaisedButton(
            child: Text('Start Screen'),
            onPressed: _onButtonPressed,
          ),
          Padding(
            padding: EdgeInsets.only(top: 40.0),
            child: Text(_responseText),
          ),
        ],
      ),
    );
  }

  void _onButtonPressed() {
    if (Platform.isAndroid) {
      _startAndroidNativeActivity();
    } else {
      setState(() {
        _responseText = 'OS not support yet.';
      });
    }
  }

  Future<void> _startAndroidNativeActivity() async {
    try {
      final String result = await MethodChannel('com.demo.flutter_kc_playground/main_channel').invokeMethod('startAndroidNativeActivity', <String, String>{'arg': _textController.text});

      setState(() {
        _responseText = 'Response: $result';
      });
    } on PlatformException catch (e) {
      print('d;; _startAndroidNativeActivity exception');
      print(e);
    }

    return;
  }
}
