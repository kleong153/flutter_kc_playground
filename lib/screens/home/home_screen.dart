import 'package:flutter/material.dart';
import 'package:flutter_kc_playground/components/app_scaffold.dart';
import 'package:flutter_kc_playground/router.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      titleText: 'Home',
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              child: Text('Start Native Screen'),
              onPressed: () => Navigator.pushNamed(context, Router.startNativeScreen),
            ),
            RaisedButton(
              child: Text('Stream List'),
              onPressed: () => Navigator.pushNamed(context, Router.streamList),
            ),
            RaisedButton(
              child: Text('Cached List'),
              onPressed: () => Navigator.pushNamed(context, Router.cachedList),
            ),
            RaisedButton(
              child: Text('DB Viewer'),
              onPressed: () => Navigator.pushNamed(context, Router.dbViewer),
            ),
          ],
        ),
      ),
    );
  }
}
