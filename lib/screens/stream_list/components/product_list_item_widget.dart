import 'package:flutter/material.dart';
import 'package:flutter_kc_playground/models/product.dart';
import 'package:meta/meta.dart';

class ProductListItemWidget extends StatelessWidget {
  final Product product;

  const ProductListItemWidget({Key key, @required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Flexible(
              child: Container(
                color: Colors.blueGrey,
                child: Center(
                  child: Text(
                    product.site,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text(
                product.title,
                maxLines: 1,
                softWrap: true,
                textAlign: TextAlign.left,
              ),
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text(
                'RM ${product.price.toStringAsFixed(2)}',
                maxLines: 1,
                softWrap: true,
                textAlign: TextAlign.left,
              ),
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text(
                '(Load seconds: ${product.delay})',
                maxLines: 1,
                softWrap: true,
                textAlign: TextAlign.left,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
