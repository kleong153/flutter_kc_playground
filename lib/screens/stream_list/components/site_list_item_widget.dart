import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class SiteListItemWidget extends StatelessWidget {
  final String siteName;
  final int productCount;

  const SiteListItemWidget({
    Key key,
    @required this.siteName,
    @required this.productCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5.0),
      child: Card(
        color: Colors.white,
        child: Text('$siteName ($productCount)'),
      ),
    );
  }
}
