import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_kc_playground/blocs/stream_list/stream_list_bloc.dart';
import 'package:flutter_kc_playground/blocs/stream_list/stream_list_event.dart';
import 'package:flutter_kc_playground/components/app_scaffold.dart';
import 'package:flutter_kc_playground/screens/stream_list/components/empty_list_widget.dart';
import 'package:flutter_kc_playground/screens/stream_list/components/product_list_item_widget.dart';
import 'package:http/http.dart' as http;

class StreamListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      titleText: 'Stream List',
      body: _BodyBuilder(),
    );
  }
}

class _BodyBuilder extends StatefulWidget {
  @override
  State<_BodyBuilder> createState() => _BodyState();
}

class _BodyState extends State<_BodyBuilder> {
  StreamListBloc _productListBloc;
  Completer<void> _refreshCompleter;

  @override
  void initState() {
    super.initState();

    _refreshCompleter = Completer<void>();

    _productListBloc = StreamListBloc(httpClient: http.Client());
    _productListBloc.event.add(StreamListRefresh());
  }

  @override
  Widget build(BuildContext context) {
    _productListBloc.loading.listen((state) {
      if (!state) {
        _refreshCompleter?.complete();
        _refreshCompleter = Completer();
      }
    });

    return StreamBuilder(
      stream: _productListBloc.products,
      builder: (context, snapshot) {
        return RefreshIndicator(
          onRefresh: () {
            _productListBloc.event.add(StreamListRefresh());

            return _refreshCompleter.future;
          },
          child: snapshot.hasData
              ? Padding(
                  padding: EdgeInsets.all(5.0),
                  child: GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                    ),
                    itemBuilder: (context, index) {
                      return ProductListItemWidget(
                        product: snapshot.data[index],
                      );
                    },
                    itemCount: snapshot.data.length,
                  ))
              : EmptyListWidget(text: 'No data.'),
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    _productListBloc.dispose();
  }
}
