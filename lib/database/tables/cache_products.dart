import 'package:moor_flutter/moor_flutter.dart';

@DataClassName('CacheProduct')
class CacheProducts extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get sid => text()();

  TextColumn get title => text()();

  TextColumn get site => text()();

  RealColumn get price => real().withDefault(const Constant(0.0))();

  DateTimeColumn get cachedAt => dateTime()();
}
