// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cache_products_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$CacheProductsDaoMixin on DatabaseAccessor<AppDatabase> {
  $CacheProductsTable get cacheProducts => attachedDatabase.cacheProducts;
}
