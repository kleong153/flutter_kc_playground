import 'package:flutter_kc_playground/database/app_database.dart';
import 'package:flutter_kc_playground/database/tables/cache_products.dart';
import 'package:meta/meta.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'cache_products_dao.g.dart';

@UseDao(tables: [CacheProducts])
class CacheProductsDao extends DatabaseAccessor<AppDatabase> with _$CacheProductsDaoMixin {
  CacheProductsDao(AppDatabase attachedDatabase) : super(attachedDatabase);

  // Variable name mapper for generated class.
  $CacheProductsTable get _table => cacheProducts;

  // Common CRUD.
  Future<List<CacheProduct>> getAllEntries() => select(_table).get();

  // Override insert method to accept pure required params.
  Future<int> _insertEntry(CacheProductsCompanion entry) => into(_table).insert(entry);

  Future<int> updateEntry(CacheProductsCompanion entry) {
    return (update(_table)..where((tbl) => tbl.id.equals(entry.id.value))).write(entry);
  }

  Future<int> deleteEntry(CacheProduct entry) => delete(_table).delete(entry);

  Future<int> deleteAllEntries() => (delete(_table)).go();

  // Custom queries.
  Future<int> insertEntry({
    @required String sid,
    @required String title,
    @required String site,
    @required double price,
  }) {
    return _insertEntry(CacheProductsCompanion.insert(
      sid: sid,
      title: title,
      site: site,
      price: Value(price),
      cachedAt: DateTime.now(),
    ));
  }

  Future<List<CacheProduct>> getEntriesWithLimit(int limit, int offset) {
    return (select(_table)..limit(limit, offset: offset)).get();
  }

  Future<bool> isCacheStillValid({
    int cacheLifespanMinutes = 10,
  }) async {
    // Get latest inserted data.
    final CacheProduct latestCache = await (select(_table)
          ..orderBy([
            (tbl) => OrderingTerm(expression: tbl.cachedAt, mode: OrderingMode.desc),
          ])
          ..limit(1)) // Must work together with getSingle().
        .getSingle();

    if (latestCache != null) {
      return latestCache.cachedAt
          .add(Duration(
            minutes: cacheLifespanMinutes,
          ))
          .isAfter(DateTime.now());
    }

    return false;
  }
}
