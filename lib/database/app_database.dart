import 'package:flutter/foundation.dart' as Foundation;
import 'package:flutter_kc_playground/database/tables/cache_products.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'app_database.g.dart';

///
/// REF: Moor official documentation:
/// https://moor.simonbinder.eu/docs/getting-started/
///
/// REF: Tutorial from:
/// https://medium.com/flutterdevs/moor-database-in-flutter-6a78d91b10e5
///
/// REF: Command for generate .g.dart file:
/// flutter packages pub run build_runner build
///
@UseMoor(tables: [
  CacheProducts,
])
class AppDatabase extends _$AppDatabase {
  AppDatabase()
      : super(FlutterQueryExecutor.inDatabaseFolder(
          path: 'app_database.sqlite',
          logStatements: true,
        ));

  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration => MigrationStrategy(
        beforeOpen: (OpeningDetails details) async {
          if (Foundation.kDebugMode) {
            // Debug mode. Delete and re-create all tables.
//            final m = createMigrator();
//
//            for (final table in allTables) {
//              await m.deleteTable(table.actualTableName);
//              await m.createTable(table);
//            }
          }
        },
        onCreate: (Migrator m) {
          return m.createAll();
        },
//        onUpgrade: (Migrator m, int from, int to) async {
//          // TODO samples
//          if (from == 1) {
//            // we added the dueDate property in the change from version 1
//            await m.addColumn(todos, todos.dueDate);
//          }
//        },
      );
}
