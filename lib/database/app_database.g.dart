// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class CacheProduct extends DataClass implements Insertable<CacheProduct> {
  final int id;
  final String sid;
  final String title;
  final String site;
  final double price;
  final DateTime cachedAt;
  CacheProduct(
      {@required this.id,
      @required this.sid,
      @required this.title,
      @required this.site,
      @required this.price,
      @required this.cachedAt});
  factory CacheProduct.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final doubleType = db.typeSystem.forDartType<double>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return CacheProduct(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      sid: stringType.mapFromDatabaseResponse(data['${effectivePrefix}sid']),
      title:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}title']),
      site: stringType.mapFromDatabaseResponse(data['${effectivePrefix}site']),
      price:
          doubleType.mapFromDatabaseResponse(data['${effectivePrefix}price']),
      cachedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}cached_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || sid != null) {
      map['sid'] = Variable<String>(sid);
    }
    if (!nullToAbsent || title != null) {
      map['title'] = Variable<String>(title);
    }
    if (!nullToAbsent || site != null) {
      map['site'] = Variable<String>(site);
    }
    if (!nullToAbsent || price != null) {
      map['price'] = Variable<double>(price);
    }
    if (!nullToAbsent || cachedAt != null) {
      map['cached_at'] = Variable<DateTime>(cachedAt);
    }
    return map;
  }

  CacheProductsCompanion toCompanion(bool nullToAbsent) {
    return CacheProductsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      sid: sid == null && nullToAbsent ? const Value.absent() : Value(sid),
      title:
          title == null && nullToAbsent ? const Value.absent() : Value(title),
      site: site == null && nullToAbsent ? const Value.absent() : Value(site),
      price:
          price == null && nullToAbsent ? const Value.absent() : Value(price),
      cachedAt: cachedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(cachedAt),
    );
  }

  factory CacheProduct.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return CacheProduct(
      id: serializer.fromJson<int>(json['id']),
      sid: serializer.fromJson<String>(json['sid']),
      title: serializer.fromJson<String>(json['title']),
      site: serializer.fromJson<String>(json['site']),
      price: serializer.fromJson<double>(json['price']),
      cachedAt: serializer.fromJson<DateTime>(json['cachedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'sid': serializer.toJson<String>(sid),
      'title': serializer.toJson<String>(title),
      'site': serializer.toJson<String>(site),
      'price': serializer.toJson<double>(price),
      'cachedAt': serializer.toJson<DateTime>(cachedAt),
    };
  }

  CacheProduct copyWith(
          {int id,
          String sid,
          String title,
          String site,
          double price,
          DateTime cachedAt}) =>
      CacheProduct(
        id: id ?? this.id,
        sid: sid ?? this.sid,
        title: title ?? this.title,
        site: site ?? this.site,
        price: price ?? this.price,
        cachedAt: cachedAt ?? this.cachedAt,
      );
  @override
  String toString() {
    return (StringBuffer('CacheProduct(')
          ..write('id: $id, ')
          ..write('sid: $sid, ')
          ..write('title: $title, ')
          ..write('site: $site, ')
          ..write('price: $price, ')
          ..write('cachedAt: $cachedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          sid.hashCode,
          $mrjc(
              title.hashCode,
              $mrjc(
                  site.hashCode, $mrjc(price.hashCode, cachedAt.hashCode))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is CacheProduct &&
          other.id == this.id &&
          other.sid == this.sid &&
          other.title == this.title &&
          other.site == this.site &&
          other.price == this.price &&
          other.cachedAt == this.cachedAt);
}

class CacheProductsCompanion extends UpdateCompanion<CacheProduct> {
  final Value<int> id;
  final Value<String> sid;
  final Value<String> title;
  final Value<String> site;
  final Value<double> price;
  final Value<DateTime> cachedAt;
  const CacheProductsCompanion({
    this.id = const Value.absent(),
    this.sid = const Value.absent(),
    this.title = const Value.absent(),
    this.site = const Value.absent(),
    this.price = const Value.absent(),
    this.cachedAt = const Value.absent(),
  });
  CacheProductsCompanion.insert({
    this.id = const Value.absent(),
    @required String sid,
    @required String title,
    @required String site,
    this.price = const Value.absent(),
    @required DateTime cachedAt,
  })  : sid = Value(sid),
        title = Value(title),
        site = Value(site),
        cachedAt = Value(cachedAt);
  static Insertable<CacheProduct> custom({
    Expression<int> id,
    Expression<String> sid,
    Expression<String> title,
    Expression<String> site,
    Expression<double> price,
    Expression<DateTime> cachedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (sid != null) 'sid': sid,
      if (title != null) 'title': title,
      if (site != null) 'site': site,
      if (price != null) 'price': price,
      if (cachedAt != null) 'cached_at': cachedAt,
    });
  }

  CacheProductsCompanion copyWith(
      {Value<int> id,
      Value<String> sid,
      Value<String> title,
      Value<String> site,
      Value<double> price,
      Value<DateTime> cachedAt}) {
    return CacheProductsCompanion(
      id: id ?? this.id,
      sid: sid ?? this.sid,
      title: title ?? this.title,
      site: site ?? this.site,
      price: price ?? this.price,
      cachedAt: cachedAt ?? this.cachedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (sid.present) {
      map['sid'] = Variable<String>(sid.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    if (site.present) {
      map['site'] = Variable<String>(site.value);
    }
    if (price.present) {
      map['price'] = Variable<double>(price.value);
    }
    if (cachedAt.present) {
      map['cached_at'] = Variable<DateTime>(cachedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CacheProductsCompanion(')
          ..write('id: $id, ')
          ..write('sid: $sid, ')
          ..write('title: $title, ')
          ..write('site: $site, ')
          ..write('price: $price, ')
          ..write('cachedAt: $cachedAt')
          ..write(')'))
        .toString();
  }
}

class $CacheProductsTable extends CacheProducts
    with TableInfo<$CacheProductsTable, CacheProduct> {
  final GeneratedDatabase _db;
  final String _alias;
  $CacheProductsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _sidMeta = const VerificationMeta('sid');
  GeneratedTextColumn _sid;
  @override
  GeneratedTextColumn get sid => _sid ??= _constructSid();
  GeneratedTextColumn _constructSid() {
    return GeneratedTextColumn(
      'sid',
      $tableName,
      false,
    );
  }

  final VerificationMeta _titleMeta = const VerificationMeta('title');
  GeneratedTextColumn _title;
  @override
  GeneratedTextColumn get title => _title ??= _constructTitle();
  GeneratedTextColumn _constructTitle() {
    return GeneratedTextColumn(
      'title',
      $tableName,
      false,
    );
  }

  final VerificationMeta _siteMeta = const VerificationMeta('site');
  GeneratedTextColumn _site;
  @override
  GeneratedTextColumn get site => _site ??= _constructSite();
  GeneratedTextColumn _constructSite() {
    return GeneratedTextColumn(
      'site',
      $tableName,
      false,
    );
  }

  final VerificationMeta _priceMeta = const VerificationMeta('price');
  GeneratedRealColumn _price;
  @override
  GeneratedRealColumn get price => _price ??= _constructPrice();
  GeneratedRealColumn _constructPrice() {
    return GeneratedRealColumn('price', $tableName, false,
        defaultValue: const Constant(0.0));
  }

  final VerificationMeta _cachedAtMeta = const VerificationMeta('cachedAt');
  GeneratedDateTimeColumn _cachedAt;
  @override
  GeneratedDateTimeColumn get cachedAt => _cachedAt ??= _constructCachedAt();
  GeneratedDateTimeColumn _constructCachedAt() {
    return GeneratedDateTimeColumn(
      'cached_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, sid, title, site, price, cachedAt];
  @override
  $CacheProductsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'cache_products';
  @override
  final String actualTableName = 'cache_products';
  @override
  VerificationContext validateIntegrity(Insertable<CacheProduct> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('sid')) {
      context.handle(
          _sidMeta, sid.isAcceptableOrUnknown(data['sid'], _sidMeta));
    } else if (isInserting) {
      context.missing(_sidMeta);
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title'], _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (data.containsKey('site')) {
      context.handle(
          _siteMeta, site.isAcceptableOrUnknown(data['site'], _siteMeta));
    } else if (isInserting) {
      context.missing(_siteMeta);
    }
    if (data.containsKey('price')) {
      context.handle(
          _priceMeta, price.isAcceptableOrUnknown(data['price'], _priceMeta));
    }
    if (data.containsKey('cached_at')) {
      context.handle(_cachedAtMeta,
          cachedAt.isAcceptableOrUnknown(data['cached_at'], _cachedAtMeta));
    } else if (isInserting) {
      context.missing(_cachedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  CacheProduct map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return CacheProduct.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $CacheProductsTable createAlias(String alias) {
    return $CacheProductsTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $CacheProductsTable _cacheProducts;
  $CacheProductsTable get cacheProducts =>
      _cacheProducts ??= $CacheProductsTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [cacheProducts];
}
