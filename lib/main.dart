import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_kc_playground/database/app_database.dart';
import 'package:flutter_kc_playground/router.dart';
import 'package:provider/provider.dart';

void main() {
  Bloc.observer = SimpleBlocObserver();
  runApp(
    Provider<AppDatabase>(
      create: (context) => AppDatabase(),
      child: MainApp(),
      dispose: (context, db) => db.close(),
    ),
  );
}

class MainApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Playground',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: Router.home,
      onGenerateRoute: Router.generateRoute,
    );
  }
}

class SimpleBlocObserver extends BlocObserver {
  // No need override anything.
}
