import 'package:equatable/equatable.dart';

abstract class StreamListEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class StreamListRefresh extends StreamListEvent {}
