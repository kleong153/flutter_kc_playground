import 'dart:async';
import 'dart:convert';

import 'package:flutter_kc_playground/blocs/stream_list/stream_list_event.dart';
import 'package:flutter_kc_playground/models/product.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

class StreamListBloc {
  final http.Client httpClient;
  final _eventController = StreamController<StreamListEvent>();
  final _productStateController = StreamController<List<Product>>();
  final _loadingStateController = StreamController<bool>();
  final _httpResponseStateController = StreamController<http.Response>();
  final List<String> _uris = [
    'https://ckapi.klangappdev.com/test-api?site=Shop1',
    'https://ckapi.klangappdev.com/test-api?site=Shop2',
    'https://ckapi.klangappdev.com/test-api?site=Shop3',
    'https://ckapi.klangappdev.com/test-api?site=Shop4',
    'https://ckapi.klangappdev.com/test-api?site=Shop5',
    'https://ckapi.klangappdev.com/test-api?site=Shop6',
  ];
  final List<Product> _products = [];

  StreamListBloc({@required this.httpClient}) {
    _eventController.stream.listen((event) {
      if (event is StreamListRefresh) {
        // TODO fetch data
        _inLoading.add(true); // Set ui to loading.

        // Clear current data and notify ui.
        _products.clear();
        _inProducts.add(_products);

        for (String uri in _uris) {
          httpClient.get(uri).then((response) {
            // Pass response to handler subject and set ui stop loading.
            _inHttpResponse.add(response);
            _inLoading.add(false);
          });
        }
      }
    });

    _httpResponse.listen((response) {
      // TODO handle http response
      List<Product> products = _getProductsFromResponse(response);

      if (products.isNotEmpty) {
        _products.addAll(products);
        _inProducts.add(_products);
      }
    });
  }

  // Event.
  Sink<StreamListEvent> get event => _eventController.sink;

  // Product state.
  StreamSink<List<Product>> get _inProducts => _productStateController.sink;

  Stream<List<Product>> get products => _productStateController.stream;

  // Loading state.
  StreamSink<bool> get _inLoading => _loadingStateController.sink;

  Stream<bool> get loading => _loadingStateController.stream;

  // Http response handler state.
  StreamSink<http.Response> get _inHttpResponse => _httpResponseStateController.sink;

  Stream<http.Response> get _httpResponse => _httpResponseStateController.stream;

  void dispose() {
    _eventController.close();
    _productStateController.close();
    _loadingStateController.close();
    _httpResponseStateController.close();
  }

  List<Product> _getProductsFromResponse(http.Response response) {
    if (response.statusCode == 200) {
      final data = (json.decode(response.body)['data']) as List;

      return data.map((e) => Product.fromJson(e)).toList();
    }

    return [];
  }
}
