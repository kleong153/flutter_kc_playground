import 'package:equatable/equatable.dart';
import 'package:flutter_kc_playground/models/product.dart';

abstract class StreamListState extends Equatable {
  const StreamListState();

  @override
  List<Object> get props => [];
}

class StreamListInitial extends StreamListState {}

class StreamListFailure extends StreamListState {}

class StreamListSuccess extends StreamListState {
  final List<Product> products;
  final bool hasReachedMax;

  const StreamListSuccess({
    this.products,
    this.hasReachedMax,
  });

  StreamListSuccess copyWith({
    List<Product> products,
    bool hasReachedMax,
  }) {
    return StreamListSuccess(
      products: products ?? this.products,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  List<Object> get props => [products, hasReachedMax];
}
