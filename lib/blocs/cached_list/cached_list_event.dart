import 'package:equatable/equatable.dart';

abstract class CachedListEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class CachedListRefresh extends CachedListEvent {
  final String search; // TODO for search sample.

  CachedListRefresh({this.search = null});

  @override
  List<Object> get props => [search];
}

class CachedListNextPage extends CachedListEvent {}
