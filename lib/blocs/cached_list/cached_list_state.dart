import 'package:equatable/equatable.dart';
import 'package:flutter_kc_playground/models/product.dart';

///
/// TODO if state is extending Equatable, return same data will not trigger bloc to update.
///
abstract class CachedListState extends Equatable {
  const CachedListState();

  @override
  List<Object> get props => [];
}

class CachedListInitial extends CachedListState {}

class CachedListLoading extends CachedListState {}

class CachedListFailure extends CachedListState {}

class CachedListSuccess extends CachedListState {
  final List<Product> products;
  final int pageIndex;
  final bool hasReachedMax;
  final String search; // TODO for search sample.

  const CachedListSuccess({
    this.products,
    this.pageIndex,
    this.hasReachedMax,
    this.search,
  });

  CachedListSuccess copyWith({
    List<Product> products,
    int pageIndex,
    bool hasReachedMax,
    String search,
  }) {
    return CachedListSuccess(
      products: products ?? this.products,
      pageIndex: pageIndex ?? this.pageIndex,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      search: search ?? this.search,
    );
  }

  @override
  List<Object> get props => [
        products,
        pageIndex,
        hasReachedMax,
        search,
      ];
}
