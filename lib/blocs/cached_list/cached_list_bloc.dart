import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter_kc_playground/blocs/cached_list/cached_list_event.dart';
import 'package:flutter_kc_playground/blocs/cached_list/cached_list_state.dart';
import 'package:flutter_kc_playground/database/app_database.dart';
import 'package:flutter_kc_playground/database/daos/cache_products_dao.dart';
import 'package:flutter_kc_playground/models/product.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';
import 'package:moor_flutter/moor_flutter.dart';

enum FetchSource { web, cache }

class CachedListBloc extends Bloc<CachedListEvent, CachedListState> {
  final http.Client httpClient;
  final AppDatabase appDatabase;
  final String url = 'https://ckapi.klangappdev.com/test-api?site=Shop1&delay=1';
  final int pageSize = 10;

  FetchSource _fetchSource;

  CachedListBloc({
    @required this.httpClient,
    @required this.appDatabase,
  }) : super(CachedListInitial());

  @override
  Stream<CachedListState> mapEventToState(CachedListEvent event) async* {
    final currentState = state;

    // TODO
    print('d;;CachedListBloc mapEventToState event');
    print(event);

    try {
      if (event is CachedListRefresh) {
        // Start working.
        yield CachedListLoading();

        // Get search param from event.
        final String search = event.search;

        if (!_isPerformingSearch(search)) {
          // Not performing search.
          // Refresh event will require clear cache.
          await _clearCache();
        }

        // Load first page.
        yield await _loadFirstPage(search);
      } else if (event is CachedListNextPage && !_hasReachedMax(currentState)) {
        // Start working.
        yield CachedListLoading();

        if (currentState is CachedListInitial) {
          // TODO check and load from cache.
          // Load first page.
          yield await _loadFirstPage(null);
        } else if (currentState is CachedListSuccess) {
          // Load next page.
          final pageIndex = currentState.pageIndex + 1;
          final data = await _fetchData(currentState.search, pageIndex, pageSize);

          yield data.isEmpty
              ? currentState.copyWith(hasReachedMax: true)
              : CachedListSuccess(
                  products: currentState.products + data,
                  pageIndex: pageIndex,
                  hasReachedMax: false,
                  search: currentState.search,
                );
        }
      }
    } catch (ex) {
      // TODO
      print('d;;CachedListBloc exception');
      print(ex);

      yield CachedListFailure();
    }
  }

  bool _hasReachedMax(CachedListState currentState) {
    return (currentState is CachedListSuccess && currentState.hasReachedMax);
  }

  Future<CachedListState> _loadFirstPage(String search) async {
    final pageIndex = 0;
    final data = await _fetchData(search, pageIndex, pageSize);

    return CachedListSuccess(
      products: data,
      pageIndex: pageIndex,
      hasReachedMax: data.isEmpty,
      search: search,
    );
  }

  Future<List<Product>> _fetchData(String search, int pageIndex, int pageSize) async {
    if (pageIndex == 0) {
      // Reset fetch source on first page.
      _fetchSource = null;
    }

    final CacheProductsDao cacheProductsDao = CacheProductsDao(appDatabase);
    final bool isPerformingSearch = _isPerformingSearch(search);

    if (_fetchSource == null) {
      // Check which source to use.
      bool isCacheStillValid = await cacheProductsDao.isCacheStillValid();

      if (isPerformingSearch) {
        // Performing search, always fetch from web.
        _fetchSource = FetchSource.web;
      } else if (!isCacheStillValid) {
        await _clearCache();

        _fetchSource = FetchSource.web;
      } else {
        _fetchSource = FetchSource.cache;
      }
    }

    if (_fetchSource == FetchSource.web) {
      // Fetch from web and store to DB.
      final response = await httpClient.get('$url&size=$pageSize&pageIndex=$pageIndex');

      if (response.statusCode == 200) {
        final data = (json.decode(response.body)['data']) as List;
        final List<Product> productList = data.map((e) => Product.fromJson(e)).toList();

        if (!isPerformingSearch) {
          // Only cache data when is not performing search.
          // Use transaction to insert data.
          appDatabase.transaction(() async {
            for (final Product product in productList) {
              await cacheProductsDao.insertEntry(
                sid: product.sid,
                site: product.site,
                title: product.title,
                price: product.price,
              );
            }
          });
        }

        return productList;
      } else {
        throw Exception('Error fetching data.');
      }
    } else {
      // Load from cache.
      final List<CacheProduct> cacheProductList = await cacheProductsDao.getEntriesWithLimit(pageSize, (pageIndex * pageSize));

      return cacheProductList.map((e) {
        return Product(
          sid: e.sid,
          site: e.site,
          title: e.title,
          price: e.price,
          delay: -1, // TODO delay = -1 to indicate data is load from cache.
        );
      }).toList();
    }
  }

  bool _isPerformingSearch(String search) {
    return !(search?.isEmpty ?? true);
  }

  Future<void> _clearCache() async {
    await CacheProductsDao(appDatabase).deleteAllEntries();
  }
}
