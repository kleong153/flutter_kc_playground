package com.demo.flutter_kc_playground

import android.app.Activity
import android.content.Intent
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

class MainActivity : FlutterActivity() {

    private val REQUEST_CODE_START_NATIVE_ACTIVITY = 100
    private val CHANNEL_START_ANDROID_ACTIVITY = "com.demo.flutter_kc_playground/main_channel";

    private var mMethodChannelResult: MethodChannel.Result? = null

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL_START_ANDROID_ACTIVITY)
                .setMethodCallHandler { call, result ->
                    if (call.method == "startAndroidNativeActivity") {
                        mMethodChannelResult = result

                        startActivityForResult(Intent(this@MainActivity, AndroidActivity::class.java).apply {
                            putExtra("arg", call.argument<String>("arg"))
                        }, REQUEST_CODE_START_NATIVE_ACTIVITY)
                    } else {
                        result.notImplemented()
                    }
                }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_START_NATIVE_ACTIVITY && resultCode == Activity.RESULT_OK) {
            mMethodChannelResult?.success(data?.getStringExtra("response"))
        }
    }
}
