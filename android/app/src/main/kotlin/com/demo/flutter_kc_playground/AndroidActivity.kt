package com.demo.flutter_kc_playground

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class AndroidActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_android)

        findViewById<TextView>(R.id.textView_arg).text = "Arg passed from flutter: ${intent.getStringExtra("arg")}";

        findViewById<Button>(R.id.button_response).setOnClickListener {
            setResult(Activity.RESULT_OK, Intent().apply {
                putExtra("response", findViewById<EditText>(R.id.editText_result).text.toString())
            })

            finish();
        }
    }
}